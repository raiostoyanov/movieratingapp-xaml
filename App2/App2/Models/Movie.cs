﻿using System;

namespace App2.Models
{
    public class Movie
    {
        public Random movieRating = new Random();
        public string starRating;
        public Movie()
        {
            MovieRating = movieRating.Next(1, 5);
            MovieRatingAsStars = starRating;
        }
        public string MovieName { get; set; }

        public int MovieRating { get; }

        public string MovieRatingAsStars
        {
            get => starRating;

            set
            {
                if (MovieRating == 1)
                {
                    starRating = "*";
                }
                else if (MovieRating == 2)
                {
                    starRating = "**";
                }
                else if (MovieRating == 3)
                {
                    starRating = "***";
                }
                else if (MovieRating == 4)
                {
                    starRating = "****";
                }
                else if (MovieRating == 5)
                {
                    starRating = "*****";
                }

            }
        }
    }

}
