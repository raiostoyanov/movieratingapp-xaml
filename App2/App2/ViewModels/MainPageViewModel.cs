﻿using App2.Models;
using App2.Views;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Xamarin.Forms;

namespace App2.ViewModels
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        public MainPageViewModel()
        {
            Movies = new ObservableCollection<Movie>();

            SaveMovieCommand = new Command(() =>
            {
                Movies.Add(item: new Movie { MovieName = Text });
                Text = string.Empty;
            },
            () => !string.IsNullOrEmpty(Text));

            DeleteMovieCommand = new Command(() => Movies.Clear());

            MovieSelectedCommand = new Command(async () =>
            {
                if (SelectedMovie is null)
                {
                    return;
                }

                DetailPageViewModel detailViewModel = new DetailPageViewModel
                {
                    Text = SelectedMovie.MovieName,
                    MovieRating = selectedMovie.MovieRatingAsStars

                };

                await Application.Current.MainPage.Navigation.PushAsync(new DetailsPage(detailViewModel));

                SelectedMovie = null;
            });
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private string text;
        public string Text
        {
            get => text;
            set
            {
                text = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Text)));

                SaveMovieCommand.ChangeCanExecute();
            }
        }

        private Movie selectedMovie;
        public Movie SelectedMovie
        {
            get => selectedMovie;
            set
            {
                selectedMovie = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(SelectedMovie)));
            }
        }

        public ObservableCollection<Movie> Movies { get; }

        public Command MovieSelectedCommand { get; }
        public Command SaveMovieCommand { get; }
        public Command DeleteMovieCommand { get; }
    }
}
